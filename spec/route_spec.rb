require_relative '../app/route'

describe Route do
	let :stops do
		[
			Stop.new('alpha', 'c1', 'task for alpha'),
			Stop.new('beta', 'c2', 'task for beta'),
			Stop.new('gama', 'c3', 'task for gama'),
		]
	end

	let :route do
		Route.new 't1', stops
	end

	it 'route starts with first stop' do
		expect(route.current_stop).to be stops[0]
  end

  context '#route_ended?' do
    it 'not in the beginning' do
      expect(route.ended?).to be false
    end
    it 'not in the middle' do
      route.check_point? 'c1'
      expect(route.ended?).to be false

      route.check_point? 'c2'
      expect(route.ended?).to be false
    end

    it 'but in the end' do
      route.check_point? 'c1'
      route.check_point? 'c2'
      route.check_point? 'c3'
      expect(route.ended?).to be true
    end
  end

  context '#last_finished' do
    it 'shows name of last finished task' do
      expect(route.last_finished).to be_nil
      route.check_point? 'c1'
      expect(route.last_finished).to eq 'alpha'
      route.check_point? 'c2'
      expect(route.last_finished).to eq 'beta'
      route.check_point? 'c3'
      expect(route.last_finished).to eq 'gama'
    end
  end
 
  context '#current_task' do
    it 'returns previous step task' do
      expect(route.current_task).to be_nil
      
      route.check_point? 'c1'
      expect(route.current_task).to eq 'task for alpha'

      route.check_point? 'c2'
      expect(route.current_task).to eq 'task for beta'

      route.check_point? 'c3'
      expect(route.current_task).to eq 'task for gama'
    end
  end

	context '#check_point?' do
    context 'correct code' do
      before :each do
        @result = route.check_point? 'c1'
      end

      it 'changes current stop' do
        expect(route.current_stop).to be stops[1]
      end

      it 'writes check time' do
        expect(stops[0].check_time).to be_within(1).of(Time.now)
      end

      it 'returns true' do
        expect(@result).to be true
      end
    end
    context 'wrong code' do
      it 'returns false' do
        expect(route.check_point? 'wrong').to be false
      end
    end
		context 'steps finished' do
			before :each do
				route.check_point? 'c1'
				route.check_point? 'c2'
				route.check_point? 'c3'
			end
			it 'returns false' do
				expect(route.check_point? 'x').to be false
			end
		end
	end

  context '#finished_stops' do
    it 'empty in the begining' do
      expect(route.finished_stops).to be_empty
    end

    it 'has first step after it\'s finished' do
      route.check_point? 'c1'
      expect(route.finished_stops).to eq [stops[0]]
    end
  end

  context '#start!' do
    it 'sets start time' do
      route.start!
      expect(route.start).to be_within(1).of(Time.new)
    end
    it 'not changes start time' do
      start_time = Time.new 2000, 1, 1
      route.start = start_time
      route.start!
      expect(route.start).to be start_time
    end
  end

end
