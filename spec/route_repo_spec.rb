require 'yaml'
require_relative '../app/route_repo'

describe RouteRepository do
	let :repo do 
		RouteRepository.new 'routes_test'
	end

	let :routes do
		route = [
			Route.new('t1',
			[
				Stop.new('Pradžia', 'a', 'Kito punkto pavadinimas yra šalis, kurios sostinė Paryžius'),
				Stop.new('Prancūzija', 'b', 'Eikite iki punkto Ispanija'),
				Stop.new('Ispanija', 'c', 'Grįžkite namo ir pasižymėkite pabaigos punkte')
			]),
			Route.new('t2',
			[
				Stop.new('Pradžia', 'a', 'Kito punkto pavadinimas yra šalis, garsi bulių kautynėmis'),
				Stop.new('Ispanija', 'c', 'Punkto pavadinimas, šalis, kurios imperatorius buvo Napoleonas'),
				Stop.new('Prancūzija', 'b', 'Grįžkite namo ir pasižymėkite pabaigos punkte')
			])
		]
    route[0].start!
    route[0].check_point? 'a'

    route
	end
	
  context '#create_competition, #for_team' do
    it 'can save/restore route' do
      repo.delete_routes
      repo.create_competition routes

      team_route = repo.for_team 't1'
      expect(YAML::dump team_route).to eq YAML::dump routes[0]

      team_route = repo.for_team 't2'
      expect(YAML::dump team_route).to eq YAML::dump routes[1]
    end
  end

	context '#update' do
		it 'saves start time' do
      repo.delete_routes
      repo.create_competition routes
      team_route = repo.for_team 't2'
			team_route.start!

			repo.update team_route

      actual = repo.for_team 't2'

			expect(YAML::dump actual).to eq YAML::dump team_route

		end
	end
end
