require 'capybara'
require 'capybara/rspec'
require 'capybara/dsl'
require 'sinatra'
require_relative '../app/app'

RSpec.configure do |cfg|
	cfg.include Capybara::DSL
end

Sinatra::Application.views = File.dirname(__FILE__) + '/../app/views'
Sinatra::Application.repository = RouteRepository.new 'routes-test'

Capybara.app = Sinatra::Application

#save_and_open_page

describe 'page navigation' do
	let(:repo) {RouteRepository.new 'routes-test' }

	context 'team has route' do
    #intialization 
		before :each do
			repo.delete_routes
			repo.create_competition [ 
				Route.new( 't1', [
					Stop.new('alpha', 'a', 't'),
					Stop.new('beta', 'b', 't'),
				]) ]

			visit '/login'
			fill_in 'team', :with => 't1'
			click_on 'post'
		end

		context '/' do
			it 'shows task description' do
				visit '/'
				#expect(page).to have_content 'alpha'
				expect(page).to have_selector 'input[name="code"]'
			end
		end

		context '/checkin' do

			context 'wrong step code' do
				it 'shows error' do
					visit '/'
					fill_in 'code', :with => 'xxx'
					click_on 'post'
					expect(page.find('#page-id').value).to eq 'wrong code'
				end
			end

			context 'corect code' do

				context 'has more steps' do
					it 'shows confirm' do
						visit '/'
						fill_in 'code', :with => 'a'
						click_on 'post'

						expect(page.find('#page-id').value).to eq 'code ok'
						expect(page).to have_content 'alpha'
					end
				end

				context 'no steps left' do
					it 'shows route end' do
						visit '/'
						fill_in 'code', :with => 'a'
						click_on 'post'
						click_on 'post'
						fill_in 'code', :with => 'b'
						click_on 'post'
						expect(page.find('#page-id').value).to eq 'route end'
					end
				end 
			end
		end
	end #context 'team has route'

	context 'team has no route' do
		it 'displays error' do
			repo.create_competition [Route.new('t1', [])]
			visit '/login'
			fill_in 'team', :with => 'bar'
			click_on 'post'

			expect(page.find('#page-id').value).to eq 'wrong team'
		end
	end
end
