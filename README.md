# About

This small web app lets organize simple orienteering game. It allows to set up each time different route with some logical task and check point via phone.

# Technology

- Sinatra as web framework
- RSpec for tests
- Capybara for UI acceptance tests

