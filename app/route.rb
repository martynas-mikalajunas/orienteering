class Stop
	def initialize(name = nil, code = nil, task = nil)
		@name, @code, @task = name, code, task
	end

  def self.from_hash(hash)
    stop = Stop.new 
    hash.each {|k, v| stop.instance_variable_set("@#{k}", v)}
    stop
  end

	attr_accessor :name, :code, :task, :check_time
end

class Route
	attr_accessor :team_id, :start, :team_name

  def self.from_hash(hash)
		return nil unless hash

    Route.new hash["team_id"], 
      hash["stops"].inject([]) {|arr, s| arr << Stop.from_hash(s)},
      hash["start"],
			hash["team_name"]
  end

	def initialize(team, stops, started = nil, team_name = nil)
		@team_id, @start, @team_name = team, started, team_name
		@stops = stops || []
	end
	
	def start!
		@start = Time.now.round(0).utc unless @start
	end

  def check_point? (code)
		return false unless current_stop && current_stop.code == code
    current_stop.check_time = Time.now.round(0).utc
    true
  end

  def finished_stops
    finished = @stops.select {|s| s.check_time }
    finished.clone
  end

  def ended? 
    @stops.all? {|s| s.check_time}
  end

  def last_finished
    name = nil
    @stops.each do |stop|
      if stop.check_time
        name = stop.name
      else
        return name
      end
    end
    name
  end

  def current_task
    task = nil
    @stops.each do |stop|
      if stop.check_time
        task = stop.task
      else
        return task
      end
    end
    task
  end

	def current_stop
		@stops.each do |step|
      return step unless step.check_time
    end
    nil
	end

end
