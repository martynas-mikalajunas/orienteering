require 'yaml'
require_relative 'route_repo'

def reset_route(file)
  repo = RouteRepository.new 

  repo.delete_routes
  repo.create_competition YAML.load_file file
end

puts 'reload routes from [pavilnio_komandos.yaml]? yes/no'
answer = gets.chomp

reset_route 'pavilnio_komandos.yaml' if answer == 'yes'
