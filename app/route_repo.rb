require 'mongo'
require_relative 'route'

class RouteRepository

	def initialize(collection_name = 'routes')
		@col_name = collection_name
    cnn = Mongo::Connection.new 'kahana.mongohq.com', 10076
    #cnn = Mongo::Connection.new 'ds039550.mongolab.com', 39550
    #cnn = Mongo::Connection.new 'localhost', 27017
    @db = cnn.db 'mm-os'
    @db.authenticate 'web', 'asd123qwe'
	end

  def for_team(team_id)
    Route.from_hash get_col.find({:team_id => team_id}).first
  end

  def delete_routes
    get_col.drop
  end

	def create_competition(routes)
    routes.each { |r| get_col.insert to_hash r} 
	end

	def update(route)
		get_col.update( {:team_id=> route.team_id}, to_hash(route))
	end

	def all_routes
		result = []
		get_col.find.each do |h|
			result << Route.from_hash(h)
		end
		result.sort_by{|r| r.team_name}
	end

  private

  def get_col
    @db[@col_name]
  end

  def to_hash(object)
    object
      .instance_variables
      .inject({}) do |hash, var| 
        name = var.to_s.gsub('@', '').to_sym
        val = object.instance_variable_get(var)
				
        if val.is_a? Enumerable 
          hash.store(name, val.inject([]) {|arr, elem| arr << to_hash(elem)})
        else
          hash.store name, val
        end
        hash
      end
  end
end

