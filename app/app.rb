require 'sinatra'
require 'haml'
require 'time_diff'
require_relative 'route_repo'


helpers do
	def get_team
		request.cookies['team']
	end
	def set_team (team_id)
		response.set_cookie 'team', :value=> team_id, :expires=> Time.new + (60*60*24)
	end
end

set :repository, RouteRepository.new('routes')

before do
	@repo = settings.repository 
  @route = @repo.for_team get_team
	redirect to '/login' unless request.path_info == '/login' || @route
end

get '/r' do
	@repo.reset
	redirect to '/'
end

get '/' do
  return haml :point_entry, :locals => {:task => @route.current_task||'įvesti punkto kodą'} unless @route.ended?
  haml :route_end, :locals => {
		:steps => @route.finished_stops.map do |s| 
			{
			 	name: s.name, 
				duration: Time.diff(s.check_time, @route.start, "%h:%m:%s")[:diff]
			}
		end	
	}
end

get '/checkin/?:code?' do
	if @route.check_point? params[:code]
		@repo.update @route
		redirect to '/confirm' 
	end
	haml :wrong_code
end

get '/confirm' do
  redirect to '/' if @route.ended?
  haml :code_ok, :locals=> {:name=> @route.last_finished}
end

get '/login' do
	haml :login
end

post '/login' do
	set_team ''
	route = @repo.for_team params[:team]
	return haml :wrong_team unless route
	set_team params[:team]
	route.start!
	@repo.update route
	redirect to '/'
end

get '/stats' do
	haml :stats, :layout=> false, :locals => {:routes => @repo.all_routes}
end
